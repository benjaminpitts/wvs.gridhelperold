﻿using System.Collections.Generic;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper
{
    public class FilterList : List<IFilter>, IFilterList
    {
        public FilterList(FilterListType type, bool? strictWhiteList = true) {
            Type = type;
            StrictWhiteList = strictWhiteList ?? true;
        }
        public FilterListType Type { get; set; }
        public bool StrictWhiteList { get; set; }
    }
}
