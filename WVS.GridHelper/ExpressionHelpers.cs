﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace WVS.GridHelper
{
    public static class ExpressionHelpers
    {
        /// <summary>
        /// Dummy expression to start an expression chain.
        /// </summary>
        public static Expression<Func<T, bool>> False<T>() => x => false;

        /// <summary>
        /// values.Contains(x.Member);
        /// Use in Where to get this SQL translation: <code>where Member in (value1, value2, ...)</code>
        /// </summary>
        /// <typeparam name="T">Table mapped type.</typeparam>
        /// <typeparam name="TValue">Column datatype of list and item to compare.</typeparam>
        /// <param name="predicate">Dummy expression to carry instance parameter, i.e. just the x in 'x => blah' will be taken.</param>
        /// <param name="memberPath">Name of column to check against list.</param>
        /// <param name="values">List of values that Member will be checked against.</param>
        /// <param name="invert">Negate output, 'not In'.</param>
        /// <returns>x => values.Contains(x.Member)</returns>
        public static Expression<Func<T, bool>> ListContainsExpression<T, TValue>
            (Expression<Func<T, bool>> predicate, string memberPath, IReadOnlyCollection<TValue> values, bool invert = false)
        {
            // x
            var param = predicate.Parameters.Single();

            // x.Member
            var memberAccessInfo = GetMemberExpressionFromPath<T>(param, memberPath);

            // .Contains()
            var contains = typeof(List<TValue>).GetMethod(nameof(List<TValue>.Contains));

            // values.Contains(x.Member)
            var methodCall = Expression.Call(Expression.Constant(values), contains, memberAccessInfo.memberExpression);

            // !values.Contains(x.Member) // if invert
            var negatedExpression = invert
                ? Expression.Not(methodCall)
                : (Expression)methodCall;

            // don't explode if something in the path is null, just return false.
            var safeComparison = SafeExpression(memberAccessInfo.nullCheckExpression, negatedExpression);

            // x => values.Contains(x.Member)
            return Expression.Lambda<Func<T, bool>>(safeComparison, param);
        }

        /// <summary>
        /// Turn nested property path into MemberExpression to access it.
        /// ALSO create a null check expression for the path leading up to the member, attach it to comparison expressions to access the member safely.
        /// </summary>
        /// <typeparam name="T">Base entity class.</typeparam>
        /// <param name="param">Parameter (the 'x' parameter in the lambda). This might not need to be passed in.</param>
        /// <param name="propertyPath">Dotted.Nested.Class.PathTo.Property</param>
        /// <returns>MemberExpression for accessing a member, and 'path is not null' expression for safe access.</returns>
        public static (MemberExpression memberExpression, Expression nullCheckExpression) GetMemberExpressionFromPath<T>(Expression param, string propertyPath)
        {
            var propertyNames = propertyPath.Split('.');
            var expression = param;
            var t = typeof(T);
            var memberExpressions = new List<MemberExpression>();
            foreach (var propertyName in propertyNames)
            {
                expression = Expression.Property(expression, t, propertyName);
                t = expression.Type;
                memberExpressions.Add((MemberExpression)expression);
            }

            Expression nullCheckExpression = null;
            foreach (var memberExpression in memberExpressions)
            {
                var nullableType = Nullable.GetUnderlyingType(memberExpression.Type);
                var typeInfo = memberExpression.Type.GetTypeInfo();
                if (typeInfo.IsValueType
                    && // isn't a nullable type, we can check for those even if they're wrapping value types.
                        null == nullableType
                    )
                    break; // value types cannot be checked for null.
                var @null = Expression.Constant(null, memberExpression.Type);
                var notNull = Expression.NotEqual(memberExpression, @null);
                nullCheckExpression = null == nullCheckExpression
                    ? notNull
                    : Expression.AndAlso(nullCheckExpression, notNull);
            }

            return (expression as MemberExpression, nullCheckExpression);
        }

        /// <summary>
        /// shortcut to wrapping an expression with a pre-constructed null-checking expression
        /// </summary>
        /// <param name="nullCheckExpression"></param>
        /// <param name="thenExpression"></param>
        /// <returns></returns>
        public static Expression SafeExpression(Expression nullCheckExpression, Expression thenExpression)
        {
            return null == nullCheckExpression
                ? thenExpression
                : Expression.AndAlso(nullCheckExpression, thenExpression);
        }
    }
}
