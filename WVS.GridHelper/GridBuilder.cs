﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace WVS.GridHelper
{
    public class GridBuild<TEntity> : GridBuilder<GridBuild<TEntity>, Grid<TEntity>, TEntity>
        where TEntity : class
    { }

    /// <summary>
    /// Use <see cref="GridBuild{TEntity}"/> instead.
    /// </summary>
    public class GridBuilder<TBuilder, TGrid, TEntity> : BuilderBase<TBuilder, TGrid>
        where TBuilder : GridBuilder<TBuilder, TGrid, TEntity>
        where TGrid : Grid<TEntity>, new()
        where TEntity : class
    {
        public TBuilder URL(string url)
        {
            Obj.DataUrl = url;
            return Builder;
        }

        public TBuilder Col<TMember>(Expression<Func<TEntity, TMember>> selector, Column.Metadata metadata = null)
        {
            var column = new Column<TEntity, TMember>(selector, metadata);
            Obj.Columns = Obj.Columns.Concat(new[] { column });
            return Builder;
        }

        public TBuilder Col<TMember>(Expression<Func<TEntity, TMember>> selector, string title, Column.Metadata metadata = null)
        {
            var column = new Column<TEntity, TMember>(selector, title, metadata);
            Obj.Columns = Obj.Columns.Concat(new[] { column });
            return Builder;
        }

        public TBuilder KeyCol<TMember>(Expression<Func<TEntity, TMember>> selector)
        {
            var metadata = new Column.Metadata
            {
                IsKey = true,
                Visible = false,
            };
            var column = new Column<TEntity, TMember>(selector, metadata);
            Obj.Columns = Obj.Columns.Concat(new[] { column });
            return Builder;
        }

        /*
        public TBuilder RemainingCols(bool hidden = false) {
            throw new NotImplementedException();
            // look thru TEntity for props not already declared as columns and add them.
            // possibly include a blacklist for leaving out specific cols by name, or a those that match a pattern (like "_ID").
            var columnsToAdd = new List<IColumn>();
            foreach (var prop in typeof(TEntity).GetProperties()) {
                if (Obj.Columns.Any(col => col.Metadata.Name == prop.Name))
                    continue;
                var memberType = prop.PropertyType;
                var parameter = Expression.Parameter(typeof(TEntity));
                var selector = Expression.MakeMemberAccess(parameter, prop);
                columnsToAdd.Add(new Column(selector));
            }
            Obj.Columns = Obj.Columns.Concat(columnsToAdd);
            return Builder;
        }
        */
    }
}
