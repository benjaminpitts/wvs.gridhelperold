﻿using System;
using System.Collections.Generic;
using System.Text;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper
{
    public class Filter : IFilter
    {
        public string Attribute { get; set; }
        public string DataType { get; set; }
        public string SelectDataType { get; set; }
        public FilterOperator Operator { get; set; }
        public List<string> Values { get; set; }
    }
}
