﻿namespace WVS.GridHelper
{
    public class BuilderBase<TBuilder, TGrid>
    where TBuilder : BuilderBase<TBuilder, TGrid>
    where TGrid : class, new()
    {
        protected TGrid Obj;
        protected TBuilder Builder;
        protected BuilderBase()
        {
            Obj = new TGrid();
            Builder = (TBuilder)this;
        }
        public TGrid Build()
        {
            var result = Obj;
            Obj = null;
            return result;
        }
    }
}
