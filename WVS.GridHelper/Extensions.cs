﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;

namespace WVS.GridHelper
{
    public static class Extensions
    {
        public static string MemberName<T, TProp>(this Expression<Func<T, TProp>> @this)
        {
            var memberExpression = @this.Body as MemberExpression;
            // TODO explodes if member is a value type being boxed into object: x => Convert(x.valueType)
            return memberExpression?.Member.Name;
        }

        /// <summary>
        /// Get path of a member expression, "A.B.C"
        /// </summary>
        public static string MemberPath<TModel, TValue>(this Expression<Func<TModel, TValue>> @this) =>
            string.Join(".", @this.MemberPathNames());

        public static IEnumerable<string> MemberPathNames<TModel, TValue>(this Expression<Func<TModel, TValue>> @this) =>
            PropertyPath<TModel>.Get(@this).Select(x => x.Name);

        public static string MemberTitle<T, TProp>(this Expression<Func<T, TProp>> @this) =>
            PascalCaseConverter.WordsFromPascalCase(@this.MemberName());

        /// <summary>
        /// Convert "PascalCaseStuffHTML" into "Pascal Case Stuff HTML" i.e. without mangling abbreviations.
        /// PascalCase splitting regex found on stackoverflow of course: https://stackoverflow.com/a/5796793/3320154
        /// There are a lot of solutions to this problem, most involving regex, this one seems to work pretty well.
        /// </summary>
        internal static class PascalCaseConverter {
            public static string WordsFromPascalCase(string name)
            {
                if (string.IsNullOrEmpty(name))
                    return string.Empty;
                return SplitPascalCaseRegexOuter.Replace(
                    SplitPascalCaseRegexInner.Replace(name, "$1 $2"),
                    "$1 $2");
            }
            private static readonly Regex SplitPascalCaseRegexInner = new Regex(@"(\P{Ll})(\P{Ll}\p{Ll})", RegexOptions.Compiled);
            private static readonly Regex SplitPascalCaseRegexOuter = new Regex(@"(\p{Ll})(\P{Ll})", RegexOptions.Compiled);
        }

        /// <summary>
        /// returns path to a property, i.e. x => x.A.B.C returns {A,B,C}
        /// thanks https://stackoverflow.com/questions/1667408/c-getting-names-of-properties-in-a-chain-from-lambda-expression.
        /// </summary>
        internal static class PropertyPath<TSource>
        {
            public static IReadOnlyList<MemberInfo> Get<TResult>(Expression<Func<TSource, TResult>> expression)
            {
                var visitor = new PropertyVisitor();
                visitor.Visit(expression.Body);
                visitor.Path.Reverse();
                return visitor.Path;
            }

            private class PropertyVisitor : ExpressionVisitor
            {
                internal readonly List<MemberInfo> Path = new List<MemberInfo>();

                protected override Expression VisitMember(MemberExpression node)
                {
                    if (!(node.Member is PropertyInfo))
                    {
                        throw new ArgumentException("The path can only contain properties", nameof(node));
                    }

                    this.Path.Add(node.Member);
                    return base.VisitMember(node);
                }
            }
        }

        /// <summary>
        /// Get value of an expression, do null checking along the path. If B is null, then x => x.A.B.C returns null instead of exploding.
        /// Modified to box value types that are null, so if C is a byte but B is null, C returns null anyway.
        /// Not helpful for construction EF expressions, but very helpful for rendering grid data where you need null values for unreachable columns.
        /// Found on stackoverflow https://stackoverflow.com/a/4281533/3320154
        /// </summary>
        public static class NullHandlingGetValue
        {
            /// <summary>
            /// Returns the value specified by the expression or Null or the default value of the expression's type if any of the items in the expression
            /// return null. Use this method for handling long property chains where checking each intermdiate value for a null would be necessary.
            /// </summary>
            public static object GetValueOrDefault<TObject, TResult>(TObject instance, Expression<Func<TObject, TResult>> expression)
                where TObject : class
            {
                return GetValue(instance, expression.Body);
            }

            private static object GetValue(object value, Expression expression)
            {
                object result;

                if (value == null) return null;

                switch (expression.NodeType)
                {
                    case ExpressionType.Parameter:
                        return value;

                    case ExpressionType.MemberAccess:
                        var memberExpression = (MemberExpression)expression;
                        result = GetValue(value, memberExpression.Expression);

                        return result == null ? null : GetValue(result, memberExpression.Member);

                    case ExpressionType.Call:
                        var methodCallExpression = (MethodCallExpression)expression;

                        if (!SupportsMethod(methodCallExpression))
                            throw new NotSupportedException($"{methodCallExpression.Method} is not supported");

                        result = GetValue(value, methodCallExpression.Method.IsStatic
                                                     ? methodCallExpression.Arguments[0]
                                                     : methodCallExpression.Object);
                        return result == null
                                   ? null
                                   : GetValue(result, methodCallExpression.Method);

                    case ExpressionType.Convert:
                        var unaryExpression = (UnaryExpression)expression;

                        return Convert(GetValue(value, unaryExpression.Operand), unaryExpression.Type);

                    default:
                        throw new NotSupportedException($"{expression.GetType()} not supported");
                }
            }

            private static object Convert(object value, Type type)
            {
                return Expression.Lambda(Expression.Convert(Expression.Constant(value), type)).Compile().DynamicInvoke();
            }

            private static object GetValue(object instance, MemberInfo memberInfo)
            {
                switch (memberInfo.MemberType)
                {
                    case MemberTypes.Field:
                        return ((FieldInfo)memberInfo).GetValue(instance);
                    case MemberTypes.Method:
                        return GetValue(instance, (MethodBase)memberInfo);
                    case MemberTypes.Property:
                        return GetValue(instance, (PropertyInfo)memberInfo);
                    default:
                        throw new NotSupportedException($"{memberInfo.MemberType} not supported");
                }
            }

            private static object GetValue(object instance, PropertyInfo propertyInfo)
            {
                return propertyInfo.GetGetMethod(true).Invoke(instance, null);
            }

            private static object GetValue(object instance, MethodBase method)
            {
                return method.IsStatic
                           ? method.Invoke(null, new[] { instance })
                           : method.Invoke(instance, null);
            }

            private static bool SupportsMethod(MethodCallExpression methodCallExpression)
            {
                return (methodCallExpression.Method.IsStatic && methodCallExpression.Arguments.Count == 1) || (methodCallExpression.Arguments.Count == 0);
            }
        }
    }
}
