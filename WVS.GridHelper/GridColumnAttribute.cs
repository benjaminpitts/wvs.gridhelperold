﻿using System;

namespace WVS.GridHelper {
    /// <summary>
    /// Maps this property to jqGrid's colModel for use with JQGrid helper.
    /// Property documentation taken from http://www.trirand.com/jqgridwiki/doku.php?id=wiki:colmodel_options.
    /// </summary>
    [Obsolete("not used, not updated to changes in grid. if attribute configuration is needed, it's possible but no longer implemented.")]
    [AttributeUsage(AttributeTargets.Property)]
    public class GridColumnAttribute : Attribute {
        public GridColumnAttribute(string title = null) {
            Title = title;
        }

        /// <summary>
        /// Overwrite the id (defined in readers) from server. Can be set as id for the unique row id. Only one column can have this property. This option have higher priority as those from the readers. If there are more than one key set the grid finds the first one and the second is ignored.
        /// </summary>
        public bool IsKey {
            get => Metadata.IsKey;
            set => Metadata.IsKey = value;
        }

        /// <summary>
        /// Display name of column, rendered to jqGrid colName.
        /// </summary>
        public string Title {
            get => Metadata.Title;
            set => Metadata.Title = value;
        }

        public string Index
        {
            get => Metadata.Index;
            set => Metadata.Index = value;
        }

        /// <summary>
        /// Defines if this column is hidden at initialization.
        /// </summary>
        public bool Visible {
            get => Metadata.Visible;
            set => Metadata.Visible = value;
        }

        /// <summary>
        /// Set the initial width of the column, in pixels. This value currently can not be set as percentage.
        /// </summary>
        public int Width {
            get => throw new InvalidOperationException($"Don't call Get on {nameof(Width)}");
            set => WidthFixed = value;
        }

        /// <summary>
        /// This is the property actually used for width. This is a workaround for attribute parameters unable to accept nullable values.
        /// </summary>
        private int? WidthFixed {
            get => Metadata.Width;
            set => Metadata.Width = value;
        }

        public int DecimalPlaces {
            get => throw new InvalidOperationException($"Don't call Get on {nameof(DecimalPlaces)}");
            set => NumberDecimalPlaces = value;
        }

        private int? NumberDecimalPlaces {
            get => Metadata.DecimalPlaces;
            set => Metadata.DecimalPlaces = value;
        }

        public Column.Metadata Metadata = new Column.Metadata();
    }
}
