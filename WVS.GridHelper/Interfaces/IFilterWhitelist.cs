﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WVS.GridHelper.Interfaces
{
    public interface IFilterWhitelist : IList<string>
    {
        string Name { get; set; }
    }
}
