﻿namespace WVS.GridHelper.Interfaces
{
    public interface IColumn
    {
        string Path { get; }
        string DynamicLinqSelector { get; }
        string Name { get; }
        Column.Metadata Meta { get; }
    }
}
