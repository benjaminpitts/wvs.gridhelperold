﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic.Core;
using System.Reflection;
using LinqKit;
using WVS.GridHelper.Interfaces;
using static WVS.GridHelper.ExpressionHelpers;

namespace WVS.GridHelper
{
    public abstract class Filterer {
        public abstract IQueryable<T> ApplyFilters<T>(IQueryable<T> @this, IFilterList filters, IFilterWhitelist whiteList, IFilterList scopeFilters);
        protected TypeFilterer DateTimeFilterer { get; set; }
    }

    public abstract class TypeFilterer
    {
        public abstract Expression<Func<T, bool>> Filter<T>(IFilter filter);
    }

    public class FilterDateTimes : TypeFilterer
    {
        public override Expression<Func<T, bool>> Filter<T>(IFilter filter) {
            throw new NotImplementedException("generic filters do not support datetimes yet.");
        }
    }

    public class LinqFilter : Filterer {
        protected TypeFilterer DatetimeFilterer = new FilterDateTimes();

        #region Process FilterList/Filter into generic Expressions

        public override IQueryable<T> ApplyFilters<T>(IQueryable<T> @this, IFilterList filters, IFilterWhitelist whiteList, IFilterList scopeFilters)
        {
            // ensure each filter matches whitelist (are allowed to be filtered against).
            CheckFiltersAgainstWhitelist(whiteList, filters);
            CheckFiltersAgainstWhitelist(whiteList, scopeFilters);

            // render all filters to Expressions.
            var predicate = AggregateFilters<T>(filters);

            // AND check single 'scope' filter list. commonly used to limit user's results to their security access, so filters can be freely changed while scope is enforced.
            // TODO support nested filters properly as a tree of items/lists.
            if (null != scopeFilters && scopeFilters.Any())
            {
                var scopePredicate = scopeFilters.Any()
                    ? AggregateFilters<T>(scopeFilters)
                    : null;
                if (null != scopePredicate)
                    predicate = predicate.And(scopePredicate); // this is AND even if the first list is OR, because the scoped FilterList should always be applied
            }

            // apply all filters to IQueryable.
            return predicate != null
                ? @this.Where(predicate)
                : @this; // no filters.
        }

        public static void CheckFiltersAgainstWhitelist(IFilterWhitelist whiteList, IFilterList filters)
        {
            if (null == whiteList || null == filters)
                return; // no filters, no whiteList, no problems.

            foreach (var filter in filters)
            {
                if (whiteList.Contains(filter.Attribute))
                    continue;
                var msg = $"Filter Attribute: {filter.Attribute} does not exist in WhiteList: {whiteList.Name}";
                if (filters.StrictWhiteList)
                    throw new Exception(msg);
            }
        }

        /// <summary>
        /// Render FilterList into expression chain that can be used in a single Where clause.
        /// </summary>
        /// <typeparam name="T">Base entity targeted by these filters.</typeparam>
        /// <param name="filters">List of Filter objects describing filters to filter by.</param>
        /// <returns>LinqKit expression suitable for using in Where clause.</returns>
        private ExpressionStarter<T> AggregateFilters<T>(IFilterList filters)
        {
            // translate Filter into Expressions.
            var predicates = filters?
                .Select(filter =>
                {
                    var p = FilterPredicate<T>(filter);
                    if (null == p)
                        throw new InvalidOperationException($"unable to process filter: {filter.Attribute}");
                    return p;
                })
                .ToList();

            if (predicates?.Any(x => x == null) ?? false)
                throw new InvalidOperationException("unable to filter.");

            // combine filter expressions into single chain (a and b and c...)
            var predicate = PredicateBuilder.New<T>(true);
            predicate = predicates?.Aggregate(predicate, (current, filterPredicate) =>
                filters.Type == FilterListType.And
                    ? current.And(filterPredicate)
                    : current.Or(filterPredicate));

            return predicate;
        }

        /// <summary>
        /// Parse a Filter into an Expression suitable for translation to SQL by EF.
        /// </summary>
        /// <typeparam name="T">Table entity type.</typeparam>
        /// <param name="filter">Filter clause to parse.</param>
        /// <returns>Lambda Expression suitable to feed to a Where() or combine with others.</returns>
        private Expression<Func<T, bool>> FilterPredicate<T>(IFilter filter)
        {
            var name = filter.Attribute;
            if (null == filter.Values || !filter.Values.Any() || string.IsNullOrEmpty(name))
                return null; // can't filter by nothing, skip filter.

            switch (filter.DataType.ToLower())
            {
                case "select":
                    throw new NotImplementedException("'select' type not implemented. Where is this used?");
                case "string":
                    return FilterStrings.Filter<T>(filter);
                case "datetime":
                    return DateTimeFilterer.Filter<T>(filter);
                case "byte":
                case "int8":
                    return (Expression<Func<T, bool>>)FilterNumbers.Filter<T, byte>(filter);
                case "short":
                case "int16":
                    return (Expression<Func<T, bool>>)FilterNumbers.Filter<T, short>(filter);
                case "int":
                case "int32":
                    return (Expression<Func<T, bool>>)FilterNumbers.Filter<T, int>(filter);
                case "long":
                case "int64":
                    return (Expression<Func<T, bool>>)FilterNumbers.Filter<T, long>(filter);
                case "float":
                case "single":
                    return (Expression<Func<T, bool>>)FilterNumbers.Filter<T, float>(filter);
                case "double":
                    return (Expression<Func<T, bool>>)FilterNumbers.Filter<T, double>(filter);
                case "decimal":
                    return (Expression<Func<T, bool>>)FilterNumbers.Filter<T, decimal>(filter);
            }
            return null; // unsupported type, skip filter.
        }
        #endregion

        #region Filters for all types
        private static class FilterStrings {
            /// <summary>
            /// strings. handle operators that translate into SQL 'LIKE' or '='.
            /// </summary>
            [SuppressMessage("ReSharper", "ArgumentsStyleLiteral"), SuppressMessage("ReSharper", "RedundantArgumentDefaultValue")]
            public static Expression<Func<T, bool>> Filter<T>(IFilter filter)
            {
                var value = filter.Values.First();
                var name = filter.Attribute;
                switch (filter.Operator)
                {
                    case FilterOperator.DOES_NOT_EQUAL:
                        return StringLike<T>(name, value, startsWith: true, endsWith: true, invert: true);
                    case FilterOperator.EQUALS:
                        return StringLike<T>(name, value, startsWith: true, endsWith: true, invert: false);
                    case FilterOperator.STARTS_WITH:
                        return StringLike<T>(name, value, startsWith: true, endsWith: false, invert: false);
                    case FilterOperator.ENDS_WITH:
                        return StringLike<T>(name, value, startsWith: false, endsWith: true, invert: false);
                    case FilterOperator.DOES_NOT_CONTAIN:
                        return StringLike<T>(name, value, startsWith: false, endsWith: false, invert: true);
                    case FilterOperator.CONTAINS:
                        return StringLike<T>(name, value, startsWith: false, endsWith: false, invert: false);
                }
                return null;
            }

            /// <summary>
            /// Handle string filters. Switch between using Equals, Contains, EndsWith, StartsWith which render to SQL as = or LIKE.
            /// </summary>
            /// <typeparam name="T">Base entity class.</typeparam>
            /// <param name="memberPath">Property name to filter. Can include dots to Reference.Nested.Class.Properties</param>
            /// <param name="value">Value to compare against.</param>
            /// <param name="startsWith">Value must begin the string.</param>
            /// <param name="endsWith">Value must end the string.</param>
            /// <param name="invert">Negate the result, for DOES_NOT_CONTAIN and DOES_NOT_EQUAL comparisons.</param>
            /// <returns>Lambda expression of member access and comparison.</returns>
            private static Expression<Func<T, bool>> StringLike<T>
                (string memberPath, string value, bool startsWith = true, bool endsWith = true, bool invert = false)
            {
                // dummy expression to grab T
                var predicate = False<T>();

                // x
                var param = predicate.Parameters.Single();

                // x.Member
                var memberAccessInfo = GetMemberExpressionFromPath<T>(param, memberPath);

                // string.Equals/Contains/StartsWith/EndsWith()
                var methodInfo = (startsWith && endsWith)
                    ? StringMethodInfo(nameof(string.Equals))
                    : (!startsWith && !endsWith)
                        ? StringMethodInfo(nameof(string.Contains))
                        : (!startsWith)
                            ? StringMethodInfo(nameof(string.EndsWith))
                            : StringMethodInfo(nameof(string.StartsWith));

                // x.Member.Equals(value)
                var methodCall = Expression.Call(memberAccessInfo.memberExpression, methodInfo, Expression.Constant(value));

                // !x.Member.Equals(value) // if invert
                var negatedExpression = invert
                    ? Expression.Not(methodCall)
                    : (Expression)methodCall;

                // don't explode if something in the path is null, just return false.
                var safeComparison = SafeExpression(memberAccessInfo.nullCheckExpression, negatedExpression);

                // x => x.Member.Equals(value)
                return Expression.Lambda<Func<T, bool>>(safeComparison, param);
            }

            /// <summary>
            /// Find a single string-param string method by name, such as string.Equals/Contains/StartsWith/EndsWith.
            /// </summary>
            /// <param name="methodName"><example>nameof(string.Equals)</example> etc</param>
            private static MethodInfo StringMethodInfo(string methodName) =>
                typeof(string).GetMethod(methodName, new[] {typeof(string)});

        }
        private static class FilterNumbers {
            public static LambdaExpression Filter<T, TValue>(IFilter filter) {
                var predicate = False<T>(); // dummy to start expression chain.
                var name = filter.Attribute;

                var numberValues = ParseEnumerable.Parse<TValue>(filter.Values).ToList();
                if (!numberValues.Any())
                    return null; // no values parsed, don't proceed with this filter.
                if (numberValues.Count != filter.Values.Count)
                    return null; // a value was not parsed, don't proceed with this filter.

                // handle CONTAINS/not, returns filter expression if handled.
                if (filter.Operator == FilterOperator.CONTAINS || filter.Operator == FilterOperator.DOES_NOT_CONTAIN) {
                    var negate = filter.Operator == FilterOperator.DOES_NOT_CONTAIN;
                    predicate = ListContainsExpression(predicate, name, numberValues, negate); // where name in values;
                    return predicate; // filter success, return predicate, don't need values anymore.
                }

                var values = numberValues.Cast<object>().ToArray();

                if (null == values || values.Length < 1)
                    return null; // must have >= 1 value, skip filter.

                switch (filter.Operator) {
                    case FilterOperator.BETWEEN:
                        if (values.Length != 2)
                            return null; // must receive 2 values for BETWEEN, skip filter.
                        return DynamicExpressionParser.ParseLambda(false, typeof(T), typeof(bool), "@0 <= {name} && {name} <= @1", values[0], values[1]);
                    case FilterOperator.GREATER_THAN:
                        return DynamicExpressionParser.ParseLambda(false, typeof(T), typeof(bool), $"{name} > @0", values[0]);
                    case FilterOperator.GREATER_THAN_OR_EQUAL_TO:
                        return DynamicExpressionParser.ParseLambda(false, typeof(T), typeof(bool), $"{name} >= @0", values[0]);
                    case FilterOperator.LESS_THAN:
                        return DynamicExpressionParser.ParseLambda(false, typeof(T), typeof(bool), $"{name} < @0", values[0]);
                    case FilterOperator.LESS_THAN_OR_EQUAL_TO:
                        return DynamicExpressionParser.ParseLambda(false, typeof(T), typeof(bool), $"{name} <= @0", values[0]);
                }
                return FilterDynamicEquals<T>(filter, values);
            }

            /// <summary>
            /// Number parsing, turn a list of strings into a list of numbers.
            /// </summary>
            private static class ParseEnumerable {
                public static IEnumerable<T> Parse<T>(IEnumerable<string> strings)
                {
                    var t = typeof(T);
                    IEnumerable<T> parsed;
                    if (t == typeof(byte))
                        parsed = ParseEnumerableByte(strings).Cast<T>();
                    else if (t == typeof(short))
                        parsed = ParseEnumerableShort(strings).Cast<T>();
                    else if (t == typeof(int))
                        parsed = ParseEnumerableInt(strings).Cast<T>();
                    else if (t == typeof(long))
                        parsed = ParseEnumerableLong(strings).Cast<T>();
                    else if (t == typeof(float))
                        parsed = ParseEnumerableFloat(strings).Cast<T>();
                    else if (t == typeof(double))
                        parsed = ParseEnumerableDouble(strings).Cast<T>();
                    else if (t == typeof(decimal))
                        parsed = ParseEnumerableDecimal(strings).Cast<T>();
                    else
                        throw new InvalidOperationException($"can't parse this type: {t}");
                    return parsed;
                }

                /// <summary>
                /// Convert strings to bytes. Parse failures are dropped silently.
                /// </summary>
                private static IEnumerable<byte> ParseEnumerableByte(IEnumerable<string> strings)
                {
                    foreach (var s in strings)
                    {
                        if (byte.TryParse(s, out var i))
                            yield return i;
                    }
                }

                /// <summary>
                /// Convert strings to shorts. Parse failures are dropped silently.
                /// </summary>
                private static IEnumerable<short> ParseEnumerableShort(IEnumerable<string> strings)
                {
                    foreach (var s in strings)
                    {
                        if (short.TryParse(s, out var i))
                            yield return i;
                    }
                }

                /// <summary>
                /// Convert strings to ints. Parse failures are dropped silently.
                /// </summary>
                private static IEnumerable<int> ParseEnumerableInt(IEnumerable<string> strings)
                {
                    foreach (var s in strings)
                    {
                        if (int.TryParse(s, out var i))
                            yield return i;
                    }
                }

                /// <summary>
                /// Convert strings to longs. Parse failures are dropped silently.
                /// </summary>
                private static IEnumerable<long> ParseEnumerableLong(IEnumerable<string> strings)
                {
                    foreach (var s in strings)
                    {
                        if (long.TryParse(s, out var i))
                            yield return i;
                    }
                }

                /// <summary>
                /// Convert strings to floats. Parse failures are dropped silently.
                /// </summary>
                private static IEnumerable<float> ParseEnumerableFloat(IEnumerable<string> strings)
                {
                    foreach (var s in strings)
                    {
                        if (float.TryParse(s, out var i))
                            yield return i;
                    }
                }

                /// <summary>
                /// Convert strings to doubles. Parse failures are dropped silently.
                /// </summary>
                private static IEnumerable<double> ParseEnumerableDouble(IEnumerable<string> strings)
                {
                    foreach (var s in strings)
                    {
                        if (double.TryParse(s, out var i))
                            yield return i;
                    }
                }

                /// <summary>
                /// Convert strings to decimals. Parse failures are dropped silently.
                /// </summary>
                private static IEnumerable<decimal> ParseEnumerableDecimal(IEnumerable<string> strings)
                {
                    foreach (var s in strings)
                    {
                        if (decimal.TryParse(s, out var i))
                            yield return i;
                    }
                }

            }
        }
        protected static LambdaExpression FilterDynamicEquals<T>(IFilter filter, object[] parsedValues)
        {
            var name = filter.Attribute;

            // handle EQUALS/DOES_NOT_EQUAL for all types.
            switch (filter.Operator)
            {
                case FilterOperator.EQUALS:
                    return DynamicExpressionParser.ParseLambda(false, typeof(T), typeof(bool), $"{name} == @0", parsedValues[0]);
                case FilterOperator.DOES_NOT_EQUAL:
                    return DynamicExpressionParser.ParseLambda(false, typeof(T), typeof(bool), $"{name} != @0", parsedValues[0]);
            }
            return null; // unhandled, skip filter.
        }
        #endregion
    }
}
