﻿using System.Linq;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper
{
    public class GridAction
    {
        public string Title;
        public string FontAwesomeIcon;
        public string JavascriptCallback;
        public bool UseConfirmDialog;
        public string ConditionColumnName;

        public string ConfirmDialogText {
            get => UseConfirmDialog
                ? _confirmDialogText
                : null;
            set => _confirmDialogText = value;
        }
        private string _confirmDialogText;

        public static GridAction Delete(string javascriptCallbackFunction = null, string description = null) => new GridAction
        {
            Title = "Delete",
            FontAwesomeIcon = "trash",
            JavascriptCallback = javascriptCallbackFunction,
            UseConfirmDialog = true,
            ConfirmDialogText = $"Are you sure you want to delete{(string.IsNullOrEmpty(description) ? string.Empty : $" {description}")}?",
        };
    }

    public class GridActionColumn {
        public string Title;
        public string JavascriptCallback;
    }

    public static class GridQueryExtensions
    {
        public static IQueryable ProjectToGrid(this IQueryable query, IGrid grid) =>
            grid.Project(query);
    }
}
