﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace WVS.GridHelper.MVCJQGrid {
    /// <summary>
    /// Serializes directly to json for custom actions used in JQGridHelper.cshtml.
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class JQGridActionModel
    {
        public string title;
        public string fontAwesomeIcon;
        public string javascriptCallback;
        public bool useConfirmDialog;
        public string confirmDialogText;
        public string conditionColumnName;
        public int? conditionColumnIndex;

        public static JQGridActionModel FromAction(IJQGrid grid, GridAction action)
        {
            var conditionColumnIndex = ConditionColumnIndex(action, grid);
            var model = new JQGridActionModel
            {
                title = action.Title,
                fontAwesomeIcon = action.FontAwesomeIcon,
                javascriptCallback = action.JavascriptCallback,
                useConfirmDialog = action.UseConfirmDialog,
                confirmDialogText = action.ConfirmDialogText?.Replace("\'", string.Empty), // sanitize for JS/HTML injection. maybe some encoding would work instead of just stripping it.
                conditionColumnIndex = conditionColumnIndex,
                conditionColumnName = conditionColumnIndex.HasValue
                    ? action.ConditionColumnName
                    : null, // eat condition if we couldn't find its index. prevent conditions only working sometimes.
            };
            return model;
        }

        /// <summary>
        /// Get index of ConditionColumnName by name from the grid model.
        /// </summary>
        private static int? ConditionColumnIndex(GridAction @this, IJQGrid grid)
        {
            return IndexOf(grid.ColNames, @this.ConditionColumnName);
        }

        private static int? IndexOf(IEnumerable<string> list, string value)
        {
            var index = 0;
            foreach (var item in list)
            {
                if (item == value)
                    return index;
                index++;
            }
            return null;
        }
    }
}
