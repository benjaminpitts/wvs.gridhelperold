﻿using System.Collections.Generic;

namespace WVS.GridHelper.MVCJQGrid
{
    public class JQGridModel {
        public IEnumerable<string> colNames { get; set; }
        public IEnumerable<JQGridColModel> colModel { get; set; }
    }
}
