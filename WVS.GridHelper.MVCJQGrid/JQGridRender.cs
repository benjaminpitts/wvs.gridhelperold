﻿using System.Web.Mvc;

namespace WVS.GridHelper.MVCJQGrid
{
    using System.Web.WebPages;

    public static class JQGridRender {
        public static HelperResult Render(this IJQGrid gridConfiguration, WebViewPage pageContext) {
            var v = new Views.Shared.JQGridPartial_();
            v.ViewContext = pageContext.ViewContext;
            return v.RenderJQGrid(pageContext, gridConfiguration);
            //new WVS.GridHelper.MVCJQGrid.App_Code.JQGridHelper_().;
        }
    }
}
