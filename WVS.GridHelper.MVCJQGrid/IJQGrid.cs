﻿using System.Collections.Generic;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper.MVCJQGrid {
    public interface IJQGrid : IGrid
    {
        string DataUrl { get; set; }
        string HtmlIdContainer { get; set; }
        string OnRowSelect { get; set; }

        string HtmlIdGrid { get; }
        string HtmlIdGridPager { get; }

        // for GridAction.ConditionColumn implementation.
        IEnumerable<string> ColNames { get; }
        string KeyName { get; }

        // jqGrid specific stuff.
        IEnumerable<string> JQGridColNames();
        IEnumerable<JQGridColModel> JQGridColModels();
        JQGridModel JQGridModel();
        IEnumerable<JQGridActionModel> JQGridActionModels();
    }
}
