﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace WVS.GridHelper.MVCJQGrid {
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class JQGridPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public int total { get; set; }
        public IEnumerable<object> rows { get; set; }
    }
}
