﻿using System.Diagnostics.CodeAnalysis;

namespace WVS.GridHelper.MVCJQGrid
{
    /// <summary>
    /// Serializes directly to json for jqGrid colModel.
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class JQGridColModel
    {
        public string name;
        public bool? sortable = true;
        public bool? editable = false;
        public string index;
        public int? width;
        public bool? @fixed;
        public bool? hidden = false;
        public string srcformat;
        public string align;
        public string formatter; // mixed: can be text or a function.
        public string cellattr; // function.
        public bool? key;
        public JQGridColModelFormatOptions formatoptions;
    }
}
