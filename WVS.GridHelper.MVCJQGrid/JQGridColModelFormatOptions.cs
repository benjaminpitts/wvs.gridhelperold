﻿using System.Diagnostics.CodeAnalysis;

namespace WVS.GridHelper.MVCJQGrid {
    /// <summary>
    /// see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:predefined_formatter
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class JQGridColModelFormatOptions {
        public int? decimalPlaces;
    }
}
